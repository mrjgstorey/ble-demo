package com.example.mrjgstorey.bledemo.AttributeSupport;

import java.util.UUID;

public abstract class Attribute implements IAttribute {

    @Override
    abstract public String getAttributeTypeName();

    @Override
    public String getName() {
        return UuidNameLookup.GetUuidName(uuidIs());
    }

    @Override
    public String getUuid() {
        return UuidHelper.BleUuidToString(uuidIs());
    }

    abstract UUID uuidIs();
}
