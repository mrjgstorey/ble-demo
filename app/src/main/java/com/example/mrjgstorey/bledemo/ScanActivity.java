package com.example.mrjgstorey.bledemo;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mrjgstorey.bledemo.ScanSupport.ScanResultRecyclerAdapter;

import java.util.List;

import static android.bluetooth.le.ScanSettings.CALLBACK_TYPE_MATCH_LOST;

public class ScanActivity extends Activity {

    private static final int REQUEST_ENABLE_BT = 0;
    private static final int SCAN_PERIOD_MS = 10000;
    private BluetoothAdapter mBluetoothAdapter;

    private ScanResultRecyclerAdapter.Callback mScanResultSelectedCallback = new ScanResultRecyclerAdapter.Callback() {
        @Override
        public void scanResultClicked(@Nullable ScanResult result) {
            if ((result != null) && result.getDevice() != null) {
                final Intent intent = new Intent(getApplicationContext(), ViewAttributesActivity.class);
                intent.putExtra(ViewAttributesActivity.EXTRAS_DEVICE, result.getDevice());
                stopScan();
                startActivity(intent);
            }
        }
    };

    private ScanResultRecyclerAdapter mScanListResultAdapter = new ScanResultRecyclerAdapter(mScanResultSelectedCallback);
    private Button mScanButton;

    private boolean mScanning = false;

    private Handler mHandler = new Handler();
    private View.OnClickListener scanButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startScan();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

// Initializes Bluetooth adapter.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            mBluetoothAdapter = bluetoothManager.getAdapter();
        } else {
            mBluetoothAdapter = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        // Initializes list view adapter
        mScanListResultAdapter.clear();
        RecyclerView recyclerView = findViewById(R.id.device_list);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(mScanListResultAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        // Initialise button
        mScanButton = findViewById(R.id.scan_button);
        mScanButton.setOnClickListener(scanButtonOnClickListener);

        // Start scanning
        startScan();
    }

    public void onScanButtonPress() {
        if (mScanning) {
            stopScan();
        } else {
            startScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopScan();
        mScanListResultAdapter.clear();
    }

    void startScan() {
        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // Clear existing results
        mScanListResultAdapter.clear();

        // Start handler to stop scanning
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, SCAN_PERIOD_MS);

        mBluetoothAdapter.getBluetoothLeScanner().startScan(mScanCallback);

        mScanning = true;
        mScanButton.setText("Stop Scan");
    }

    void stopScan() {
        mBluetoothAdapter.getBluetoothLeScanner().stopScan(mScanCallback);
        mScanning = false;
        mScanButton.setText("Start Scan");
    }

    private ScanCallback mScanCallback =
            new ScanCallback() {

                @Override
                public void onScanResult(final int callbackType, final ScanResult result) {
                    super.onScanResult(callbackType, result);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (callbackType != CALLBACK_TYPE_MATCH_LOST) {
                                mScanListResultAdapter.addScanResult(result);
                            } else {
                                mScanListResultAdapter.lostScanResult(result);
                            }
                        }
                    });
                }

                @Override
                public void onBatchScanResults(final List<ScanResult> results) {
                    super.onBatchScanResults(results);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for (ScanResult sr : results) {
                                mScanListResultAdapter.addScanResult(sr);
                            }
                        }
                    });
                }

                @Override
                public void onScanFailed(int errorCode) {
                    super.onScanFailed(errorCode);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mScanning = false;
                            mScanButton.setText("Start Scan");
                        }
                    });
                }
            };

}
