package com.example.mrjgstorey.bledemo.AttributeDisplay;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mrjgstorey.bledemo.AttributeSupport.IAttribute;
import com.example.mrjgstorey.bledemo.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AttributeRecyclerAdapter extends RecyclerView.Adapter<AttributeRecyclerAdapter.ViewHolder> {

    public class AttributeAndChildren {
        IAttribute mAttribute;
        public AttributeRecyclerAdapter children = new AttributeRecyclerAdapter();

        AttributeAndChildren(IAttribute attribute) {
            mAttribute = attribute;
        }
    }

    private boolean canMinimize = false;

    private List<AttributeAndChildren> mAttributeList = Collections.synchronizedList(new ArrayList<AttributeAndChildren>());

    public AttributeRecyclerAdapter() {
        this(false);
    }

    public AttributeRecyclerAdapter(boolean canMinimize) {
        this.canMinimize = canMinimize;
    }

    public AttributeAndChildren add(@NonNull IAttribute attribute) {
        AttributeAndChildren attributeAndChildren = new AttributeAndChildren(attribute);
        mAttributeList.add(attributeAndChildren);
        this.notifyItemInserted(getItemCount() - 1);
        return attributeAndChildren;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mAttributeAndChildren = mAttributeList.get(position);

        if (holder.mAttributeAndChildren != null) {
            holder.mAttributeType.setText(holder.mAttributeAndChildren.mAttribute.getAttributeTypeName());
            holder.mAttributeName.setText(holder.mAttributeAndChildren.mAttribute.getName());
            holder.mAttributeUuid.setText(holder.mAttributeAndChildren.mAttribute.getUuid());

            // use a linear layout manager
            LinearLayoutManager layoutManager = new LinearLayoutManager(holder.mRecyclerView.getContext());
            holder.mRecyclerView.setLayoutManager(layoutManager);

            holder.mRecyclerView.setAdapter(holder.mAttributeAndChildren.children);

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(holder.mRecyclerView.getContext(),
                    layoutManager.getOrientation());
            holder.mRecyclerView.addItemDecoration(dividerItemDecoration);
        } else {
            holder.mAttributeType.setText("[null]");
            holder.mAttributeName.setText("[null]");
            holder.mAttributeUuid.setText("[null]");
        }
    }

    public void clear() {
        mAttributeList.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attribute_view, parent, false);
        return new ViewHolder(v);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mAttributeList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        boolean mMinimized = canMinimize;
        TextView mAttributeType;
        TextView mAttributeName;
        TextView mAttributeUuid;
        RecyclerView mRecyclerView;
        AttributeAndChildren mAttributeAndChildren;

        ViewHolder(View v) {
            super(v);
            mAttributeType = v.findViewById(R.id.attributeType);
            mAttributeName = v.findViewById(R.id.attributeName);
            mAttributeUuid = v.findViewById(R.id.attributeUuid);
            mRecyclerView = v.findViewById(R.id.subItems);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (canMinimize) {
                mMinimized = !mMinimized;
                ViewGroup.LayoutParams lp = mRecyclerView.getLayoutParams();
                if (mMinimized) {
                    lp.height = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT;
                } else {
                    lp.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
                }
                mRecyclerView.setLayoutParams(lp);
            }
        }
    }
}