package com.example.mrjgstorey.bledemo.AttributeSupport;

import android.support.annotation.NonNull;

import java.util.UUID;

public class UuidHelper {
    private static final UUID BLE_BASE_UUID = UUID.fromString("00000000-0000-1000-8000-00805F9B34FB");
    private static final long BLE_BASE_UUID_MSB_MASK = 0xFFFF0000FFFFFFFFL;

    public static String BleUuidToString(@NonNull UUID uuid) {
        if (is16BitUuid(uuid)) {
            return get16BitUuidString(uuid);
        } else {
            return uuid.toString();
        }
    }

    private static boolean is16BitUuid(UUID uuid) {
        // A uuid is a 16-bit UUID if the base UUID is the BLE base UUID.
        return ((uuid.getLeastSignificantBits() == BLE_BASE_UUID.getLeastSignificantBits()) &&
                ((uuid.getMostSignificantBits() & BLE_BASE_UUID_MSB_MASK) == (BLE_BASE_UUID.getMostSignificantBits())));
    }

    private static String get16BitUuidString(UUID uuid) {
        return "0x" + uuid.toString().substring(4, 8);
    }
}
