package com.example.mrjgstorey.bledemo.ScanSupport;

import android.bluetooth.le.ScanResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mrjgstorey.bledemo.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScanResultRecyclerAdapter extends RecyclerView.Adapter<ScanResultRecyclerAdapter.ViewHolder> {

    private Callback mScanResultCallback;

    public ScanResultRecyclerAdapter(Callback callback) {
        mScanResultCallback = callback;
    }

    private List<ScanResultAndStatus> mScanResultList = Collections.synchronizedList(new ArrayList<ScanResultAndStatus>());

    public void addScanResult(@NonNull ScanResult scanResult) {
        for (ScanResultAndStatus srs : mScanResultList) {
            if (srs.mScanResult.getDevice().getAddress().equals(scanResult.getDevice().getAddress())) {
                return;
            }
        }
        mScanResultList.add(new ScanResultAndStatus(scanResult));
        this.notifyItemInserted(getItemCount() - 1);
    }

    public void lostScanResult(@NonNull ScanResult scanResult) {
        for (int i = 0; i < getItemCount(); i++) {
            ScanResultAndStatus srs = mScanResultList.get(i);
            if (srs.mScanResult.getDevice().getAddress().equals(scanResult.getDevice().getAddress())) {
                srs.mLost = true;
                this.notifyItemChanged(i);
                return;
            }
        }

        /* Item not already in list */
        ScanResultAndStatus srs = new ScanResultAndStatus(scanResult);
        srs.mLost = true;
        mScanResultList.add(srs);
        this.notifyItemInserted(getItemCount() - 1);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mDeviceName.setText(getDeviceName(mScanResultList.get(position)));
        holder.mMacAddress.setText(getMacAddress(mScanResultList.get(position)));
        if (mScanResultList.get(position) != null) {
            holder.mScanResult = mScanResultList.get(position).mScanResult;
        } else {
            holder.mScanResult = null;
        }
    }

    public interface Callback {
        void scanResultClicked(@Nullable ScanResult result);
    }

    private static @NonNull String getMacAddress(@Nullable ScanResultAndStatus srs) {
        if (srs == null) {
            return "null ScanResultAndStatus";
        }

        if (srs.mScanResult == null) {
            return "null ScanResult";
        }

        if (srs.mScanResult.getDevice() == null) {
            return "null Device";
        }

        if (srs.mScanResult.getDevice().getAddress() == null) {
            return "null Address";
        }

        return srs.mScanResult.getDevice().getAddress();
    }

    private static @NonNull String getDeviceName(@Nullable ScanResultAndStatus srs) {
        if (srs == null) {
            return "null ScanResultAndStatus";
        }

        if (srs.mScanResult == null) {
            return "null ScanResult";
        }

        if (srs.mScanResult.getScanRecord() == null) {
            return "null ScanRecord";
        }

        if (srs.mScanResult.getScanRecord().getDeviceName() == null) {
            return "[No name]";
        }

        return srs.mScanResult.getScanRecord().getDeviceName();
    }

    public void clear() {
        mScanResultList.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.scan_result_view, parent, false);
        return new ViewHolder(v);
    }

    class ScanResultAndStatus {
        ScanResult mScanResult;
        boolean mLost = false;

        ScanResultAndStatus(ScanResult mScanResult) {
            this.mScanResult = mScanResult;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mScanResultList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        TextView mDeviceName;
        TextView mMacAddress;
        ScanResult mScanResult;

        ViewHolder(View v) {
            super(v);
            mDeviceName = v.findViewById(R.id.deviceName);
            mMacAddress = v.findViewById(R.id.macAddress);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mScanResultCallback != null) {
                mScanResultCallback.scanResultClicked(mScanResult);
            }
        }
    }
}