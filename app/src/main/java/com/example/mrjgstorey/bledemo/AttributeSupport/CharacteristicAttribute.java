package com.example.mrjgstorey.bledemo.AttributeSupport;

import android.bluetooth.BluetoothGattCharacteristic;

import java.util.UUID;

public class CharacteristicAttribute extends Attribute {
    private BluetoothGattCharacteristic mCharacteristic;

    public CharacteristicAttribute(BluetoothGattCharacteristic characteristic) {
        this.mCharacteristic = characteristic;
    }

    @Override
    public String getAttributeTypeName() {
        return "Characteristic";
    }

    @Override
    UUID uuidIs() {
        return mCharacteristic.getUuid();
    }

    @Override
    public int getIndent() {
        return 16;
    }
}
