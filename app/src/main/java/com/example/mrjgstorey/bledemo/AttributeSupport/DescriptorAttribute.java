package com.example.mrjgstorey.bledemo.AttributeSupport;

import android.bluetooth.BluetoothGattDescriptor;

import java.util.UUID;

public class DescriptorAttribute extends Attribute {
    private BluetoothGattDescriptor mDescriptor;

    public DescriptorAttribute(BluetoothGattDescriptor descriptor) {
        this.mDescriptor = descriptor;
    }

    @Override
    public String getAttributeTypeName() {
        return "Descriptor";
    }

    @Override
    UUID uuidIs() {
        return mDescriptor.getUuid();
    }

    @Override
    public int getIndent() {
        return 32;
    }
}
