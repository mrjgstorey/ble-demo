package com.example.mrjgstorey.bledemo;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mrjgstorey.bledemo.AttributeDisplay.AttributeRecyclerAdapter;
import com.example.mrjgstorey.bledemo.AttributeSupport.Attribute;
import com.example.mrjgstorey.bledemo.AttributeSupport.CharacteristicAttribute;
import com.example.mrjgstorey.bledemo.AttributeSupport.DescriptorAttribute;
import com.example.mrjgstorey.bledemo.AttributeSupport.IAttribute;
import com.example.mrjgstorey.bledemo.AttributeSupport.ServiceAttribute;

public class ViewAttributesActivity extends Activity {
    private final static String TAG = ViewAttributesActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE = "DEVICE";

    public static final String ACTION_GATT_CONNECTED = "GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = "GATT_DISCONNECTED";
    public static final String ACTION_GATT_SERVICES_DISCOVERED = "GATT_SERVICES_DISCOVERED";

    private BluetoothDevice mDevice;
    private View.OnClickListener mDisconnectOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // Go back to previous activity
            finish();
        }
    };

    private BluetoothGatt mGatt;
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            final Intent intent = new Intent(ACTION_GATT_SERVICES_DISCOVERED);
            sendBroadcast(intent);
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == BluetoothGatt.STATE_CONNECTED) {
                final Intent intent = new Intent(ACTION_GATT_CONNECTED);
                sendBroadcast(intent);
            } else if ((newState == BluetoothGatt.STATE_DISCONNECTING) || (newState == BluetoothGatt.STATE_DISCONNECTED)) {
                final Intent intent = new Intent(ACTION_GATT_DISCONNECTED);
                sendBroadcast(intent);
            }
        }
    };


    private BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (action != null) {
                    switch (action) {
                        case ACTION_GATT_SERVICES_DISCOVERED:
                            Toast.makeText(getApplicationContext(), "Services Discovered", Toast.LENGTH_SHORT).show();
                            displayServices();
                            mGatt.disconnect();
                            break;
                        case ACTION_GATT_CONNECTED:
                            Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
                            mGatt.discoverServices();
                            break;
                        case ACTION_GATT_DISCONNECTED:
                            Toast.makeText(getApplicationContext(), "Disconnected", Toast.LENGTH_SHORT).show();
                            mGatt.close();
                            mGatt = null;
                            break;
                    }
                }
            }
        }
    };
    private AttributeRecyclerAdapter mAttributeListAdapter = new AttributeRecyclerAdapter(true);

    private void displayServices() {
        for (BluetoothGattService s : mGatt.getServices()) {
            AttributeRecyclerAdapter.AttributeAndChildren serviceItem = mAttributeListAdapter.add(new ServiceAttribute(s));
            for (BluetoothGattCharacteristic c : s.getCharacteristics()) {
                AttributeRecyclerAdapter.AttributeAndChildren characteristicItem = serviceItem.children.add(new CharacteristicAttribute(c));
                for (BluetoothGattDescriptor d : c.getDescriptors()) {
                    characteristicItem.children.add(new DescriptorAttribute(d));
                }
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attributes);

        final Intent intent = getIntent();
        mDevice = intent.getParcelableExtra(EXTRAS_DEVICE);

        // Fill in name and address
        if (mDevice!= null) {
            ((TextView) findViewById(R.id.device_name)).setText(mDevice.getName());
            ((TextView) findViewById(R.id.mac_address)).setText(mDevice.getAddress());
        }

        // Initializes list view adapter
        mAttributeListAdapter.clear();
        RecyclerView recyclerView = findViewById(R.id.attribute_view);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(mAttributeListAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);



        Button disconnectButton = findViewById(R.id.disconnect_button);
        disconnectButton.setOnClickListener(mDisconnectOnClickListener);

        mGatt = mDevice.connectGatt(this, true, mGattCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* Register for updates */
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mGatt != null) {
            mGatt.close();
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_GATT_CONNECTED);
        intentFilter.addAction(ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(ACTION_GATT_SERVICES_DISCOVERED);
        return intentFilter;
    }
}
