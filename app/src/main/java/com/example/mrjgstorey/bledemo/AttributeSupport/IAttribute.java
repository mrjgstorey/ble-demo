package com.example.mrjgstorey.bledemo.AttributeSupport;

public interface IAttribute {
    String getAttributeTypeName();

    String getName();

    String getUuid();

    int getIndent();
}
