package com.example.mrjgstorey.bledemo.AttributeSupport;

import android.bluetooth.BluetoothGattService;

import java.util.UUID;

public class ServiceAttribute extends Attribute {
    private BluetoothGattService mService;

    public ServiceAttribute(BluetoothGattService mService) {
        this.mService = mService;
    }

    @Override
    public String getAttributeTypeName() {
        return "Service";
    }

    @Override
    UUID uuidIs() {
        return mService.getUuid();
    }

    @Override
    public int getIndent() {
        return 0;
    }
}
