# Product Requirements

> **Bluetooth**
>
> Write an example android (or desktop C++ if you prefer) application to query and list
> 
> Bluetooth devices and their services/characteristics/descriptors in a user interface."

# Plan
- Use Android Studio
- Start with a generated framework
- Implement the scan screen
- Implement the services screen
- Refine as appropriate

# Software Spec

## Scan Screen
- The first screen shown when the app is started
- The user will be able to press the Scan button to scan for nearby devices
- Any discovered devices will be added to a list
- The list will be cleared when the scan button is pressed
- The Scan button will change to "Scanning...", and will not be enabled while scanning is ongoing
- The user will be able to select an device on the list, to connect to that device.
- When the user selects a device, the Services Screen will be displayed

## The Services Screen
- The device's name (or MAC address if the device has no name) will be displayed at the top of the screen
- The connection state (connected/paired/disconnected) will be displayed under the device name
- When the app connects to a device, it will perform service discovery
- Any services and characteristics/descriptors which are discovered will be added to a list
- Initially, only the services will be displayed on the list.
- When the user selects a service on the list, a list of characteristics and descriptors associated with that service will be displayed in the list
- When the user selects that same service again, the list of characteristics/descriptors associated with that service will be hidden
- For services/characteristics/descriptors (attributes):
    - For recognised attributes, the attribute name will be shown on the list
    - For unrecognised attributes, the attribute name will be set to "[Unknown]"
    - The attribute UUID will be shown on the list
- Pressing 'back' will disconnect from the device (if the device is connected) and return the user to the scan screen.
